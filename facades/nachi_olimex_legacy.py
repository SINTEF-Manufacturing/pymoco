# coding=utf-8

"""
Module for a legacy facade to the OLIMEX-based Nachi controllers.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2019"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np

import pymoco.kinematics
import pymoco.robots
from ..input.nachi_olimex_publisher import NachiOlimexPublisher
from ..control.facilities import NachiOlimexControlFacility

from .legacy_facade import LegacyFacade


class NachiOlimexLegacyFacade(LegacyFacade):
    """ Facade for the motion generator interaction with the
    Olimex-based Nachi controllers.
    """
    def __init__(self, **kwargs):
        self._log_level = kwargs.get('log_level', 2)
        LegacyFacade.__init__(self, **kwargs)
        self._robot_type = kwargs.get('rob_type')
        self._rob_def = pymoco.robots.get_robot_type(
            self._robot_type)(**kwargs)
        # self._q_zero_offsets = kwargs.get('q_zero_offsets',
        # self._rob_def.q_zero_offsets)
        self._frame_computer = pymoco.kinematics.FrameComputer(
            rob_def=self._rob_def)
        self._olimex_host = kwargs.get('olimex_host', '127.0.0.1')
        self._bind_host = kwargs.get('bind_host', '0.0.0.0')
        self._olimex_in_port = kwargs.get('olimex_in_port',
                                          pymoco.control.llc_in_port)
        self._olimex_out_port = kwargs.get('olimex_out_port',
                                           pymoco.input.llc_out_port)
        # Start control facility
        self._control = NachiOlimexControlFacility(
            robot_facade=self,
            olimex_in_port=self._olimex_in_port,
            olimex_host=self._olimex_host)
        # Send an initialization packet for initializing the listener
        self._log('Sending initialization packet to host "{}" on port {}'
                  .format(self._olimex_host, self._olimex_in_port), 2)
        self._control._send_enc_plus(np.zeros(6, dtype=np.int32))
        # Start the listener
        self._llcp = NachiOlimexPublisher(
            robot_facade=self,
            bind_host=self._bind_host,
            olimex_host=self._olimex_host,
            olimex_out_port=self._olimex_out_port)
        # Send an initialization packet for starting listener main
        # loop, and starting the controller
        self._log('Sending initialization packet to host "{}" on port {}'
                  .format(self._olimex_host, self._olimex_in_port), 2)
        self._control._send_enc_plus(np.zeros(6, dtype=np.int32))

    def get_main_joint_enc_pos(self):
        """In case of the legacy interface to the Olimex controller,
        the joint encoder values commanded from the main controller
        must be used to compute deltas when commanding the robot.
        """
        return self._llcp._p_main_encoders
    main_joint_enc_pos = property(get_main_joint_enc_pos)

    def start(self):
        self._llcp.start()
        self._control.initialize()

    def stop(self, join=False):
        self._log('Stopping robot facade.')
        self._llcp.stop()
        if join:
            self._llcp.join()
        del self._llcp
        del self._control
