# coding=utf-8

"""
Initialization of control package.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2014"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"


import sys

import numpy as np

from .jointlinearcontroller import JointLinearController
from .jointvelocitycontroller import JointVelocityController
from .toollinearcontroller import ToolLinearController
from .toolcorrectioncontroller import ToolCorrectionController
from .pathcorrectioncontroller import PathCorrectionController
from .toolvelocitycontroller import ToolVelocityController
from .zerovelocitycontroller import ZeroVelocityController

# Ensure thread switching with high time resolution. (System default
# may be as high as 5e-3)
sys.setswitchinterval(1e-5)

llchost = ''
llc_in_port = 20021
