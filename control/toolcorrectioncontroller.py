# coding=utf-8

"""
Module for tool correction control on linear motion.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2019"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


from .correctioncontroller import CorrectionController


class ToolCorrectionController(CorrectionController):
    ''' Class implementing a linear tool controller with real-time
    tool correction capability.'''

    def __init__(self, **kwargs):
        CorrectionController.__init__(self, **kwargs)

    def _correct(self, corr, xform):
        return xform * corr

    def _un_correct(self, corr, xform):
        return xform * corr.inverse

    set_tool_orrection = CorrectionController._set_correction
