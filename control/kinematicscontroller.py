# coding=utf-8

"""
Module implementing a base class for kinematics based motion
controller.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2014"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import math3d as m3d

from .controller import Controller

class KinematicsController(Controller):
    """A version of the base Controller which offers a FrameComputer
    and exposes its methods to set a tool transform.
    """

    def __init__(self, **kwargs):
        """Use the named argument "toolXForm" for setting a tool
        transform to the kinematics computation.
        """
        Controller.__init__(self, **kwargs)
        tool_xform = kwargs.get('toolXForm', None)
        if not tool_xform is None:
            print('!! Deprecation warning: In KinematicsController.__init__:'
                  + '"toolXForm" -> "tool_xform"')
        # Override deprecated tool transform argument by correct one
        tool_xform = kwargs.get('tool_xform', tool_xform)
        if not tool_xform is None:
            self._frame_comp.tool_xform = tool_xform
