# coding=utf-8
"""
Package for implementation of control facilities. A control facility
is a collection of facilities, which may be highly specific to a given
robot system.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2013"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

from .nachi_olimex import NachiOlimexControlFacility
from .ur_gw import URGwControlFacility
