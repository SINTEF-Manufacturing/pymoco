# coding=utf-8
"""
Module for control communication functionality for the OLIMEX-extended Nachi controllers.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2013"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import time
import traceback
import struct
import socket

import numpy as np

import pymoco.control

from .control_facility import ControlFacility

class NachiOlimexControlFacility(ControlFacility):
    """A controller facility class providing some simple common mechanism
    to receive LLC data from the LLC publisher and send joint
    updates to the LLC."""

    p_struct_angles = struct.Struct('<6d')
    ## Not as simple as: p_struct_encoders = struct.Struct('<6L')
    
    class Error(Exception):
        """Error class for thowing exceptions."""
        def __init__(self, message):
            Exception.__init__(self)
            self.message = message
        def __repr__(self):
            return self.__class__.__name__ + ' : ' + self.message

    def __init__(self, **kwargs):
        """Use the given llc publisher for reading input and open a
        socket to write to the LLC on the given
        'llchost' address and 'llcInPort'."""
        ControlFacility.__init__(self, **kwargs)
        self._angles_io = kwargs.get('angles_io', False)
        self._olimex_in_port = kwargs.get('olimex_in_port',
                                          pymoco.control.llc_in_port)
        self._olimex_host = kwargs.get('olimex_host', '127.0.0.1')
        self._olimex_address = (self._olimex_host, self._olimex_in_port)
        self._olimex_in_socket = socket.socket(socket.AF_INET,
                                               socket.SOCK_DGRAM)

    def _log(self, msg, level=2):
        if level <= self._log_level:
            print(self.__class__.__name__ \
                  + '::' + traceback.extract_stack()[-2][2] + ' : ' + msg)
    
    def _scale_dq_enc(self, dq_enc):
        """Scale an actuator kinematics joint increment in radians into
        allowed speed limits according to the interpolation period
        estimate from the LLC publisher."""
        scale = 1.0
        for i, dqi, dqilim in zip(range(6)
            , dq_enc, self._rob_fac.control_period * self._rob_def.spd_lim_enc):
            if abs(dqi) > dqilim:
                scalei = dqilim / abs(dqi)
                if scalei < scale:
                    iscale = i
                    scale = scalei
        if scale < 1.0:
            if self._log_level >= 3:
                self._log('Got a max scale %.2f on joint %d'%(scale, iscale), 3)
        return scale * dq_enc
        
    def _scale_dq_act(self, dq_req_act):
        """Scale an actuator kinematics joint increment in radians into
        allowed speed limits according to the interpolation period
        estimate from the LLC publisher."""
        if not self._initialized.is_set():
            self._initialized.wait()
        d_t = self._rob_fac.control_period
        dq_mod = dq_req_act.copy()
        if self._log_level >= 5: logstr = ''
        if not self._rob_def.spd_lim_act is None:
            # Scale the step by a global factor so every component is
            # within limits.
            vreq = dq_mod / d_t
            vscaled = vreq / self._rob_def.spd_lim_act
            vfactor = np.max(np.abs(vscaled))
            if vfactor > 1.0:
                if self._log_level >=5:
                    logstr += 'v'
                dq_mod /= vfactor
        if not self._rob_def.acc_lim_act is None:
            # Clip the step, truncating it within bounds od acceleration.
            acc_lim_hi = d_t * (
                self._v_tracked + self._rob_def.acc_lim_act * d_t)
            acc_lim_low = d_t * (
                self._v_tracked - self._rob_def.acc_lim_act * d_t) 
            dq_mod_acc = np.clip(dq_mod, acc_lim_low, acc_lim_hi)
            if self._log_level >= 5 and (dq_mod_acc != dq_mod).any():
                logstr += 'a'
            dq_mod = dq_mod_acc
        if self._log_level >=5:
            if (dq_mod == dq_req_act).all():
                logstr += '-'
            self._log(logstr, 5)
        return dq_mod
    
    def _scale_dq_ser(self, dq_ser):
        """Scale a serial kinematics joint increment in radians into
        allowed speed limits according to the interpolation period
        estimate from the LLC publisher."""
        return self._rob_def.actuator2serial( 
            self._scale_dq_act(
                self._rob_def.serial2actuator(dq_ser)))

    def _set_serial_config(self, q_ser=None, do_scale=True):
        """Set the joint configuration in serial kinematics and given
        in radians. No joint speed limit check is done."""
        if not self._initialized.is_set():
            self._initialized.wait()
        if q_ser is None:
            q_ser = self._q_tracked
        elif do_scale:
            dq_ser = q_ser - self._q_tracked
            dq_ser_scaled = self._scale_dq_ser(dq_ser)
            q_ser = self._q_tracked + dq_ser_scaled
        self._update_tracking(q_ser)
        self._set_actuator_config(self._rob_def.serial2actuator(q_ser)
                                , update_track=False)
            
    def _set_actuator_config(self, q_act, update_track=True):
        """Set the actuator configuration in actuator kinematics. """
        if not self._initialized.is_set():
            self._initialized.wait()
        if update_track:
            self._update_tracking(self._rob_def.actuator2serial(q_act))
        if self._angles_io:
            apkg = self.p_struct_angles.pack(*q_act)
            self._olimex_in_socket.sendto(
                apkg, (self._olimex_host, self._olimex_in_port))
        else:
            self.__set_encoder_config(self._rob_def.actuator2encoder(q_act))
        
    def __set_encoder_config(self, q_enc):
        """Directly set the encoder configuration. No check is done on
        joint speed violations."""
        if self._angles_io:
            self._set_actuator_config(self._rob_def.encoder2actuator(q_enc))
        else:
            ## Compute the deltas (plus and minus) from desired encoder
            ## configuration by subtracting the commanded encoder
            ## configuration in the main controller
            enc_plus = q_enc - self._rob_fac.main_joint_enc_pos
            self._send_enc_plus(enc_plus)

    def _send_enc_plus(self, enc_plus):
        enc_minus = -enc_plus
        # Put into package
        pkg = struct.pack(
            '<12i', 
            *np.append(enc_plus, enc_minus).round().astype(np.int32))
        # pkg = bytearray()
        # for i, ep in enumerate(enc_plus):
        #     #pkg += struct.pack('<BBi', 0x22+i, 1, int(round(ep)))
        # for i, em in enumerate(enc_minus):
        #     pkg += struct.pack('<BBi', 0x2A+i, 2, int(round(em)))    
        self._olimex_in_socket.sendto(
                pkg, (self._olimex_host, self._olimex_in_port))

    def _set_serial_increment(self, dq_ser, do_scale=True):
        """Add the joint increment 'dq_ser' in radians in the serial
        kinematics to the most recent joint configuration. The
        increment is optionally scaled into allowed joint speed
        limits. Return value is the actually sent increment."""
        if not self._initialized.is_set():
            self._initialized.wait()
        dq_ser_scaled = dq_ser
        if do_scale:
            dq_ser_scaled = self._scale_dq_ser(dq_ser)
        self._set_serial_config(self._q_tracked + dq_ser_scaled, 
                                do_scale=False)
        return dq_ser_scaled

    def _set_actuator_increment(self, dq_act, do_scale=True):
        """Add the joint increment 'dq_act' in radians in the actuator
        kinematics to the most recent joint configuration. The
        increment is optionally scaled into allowed joint speed
        limits. Return value is the actually sent increment."""
        if not self._initialized.is_set():
            self._initialized.wait()
        dq_act_scaled = dq_act
        if do_scale:
            dq_act_scaled = self._scale_dq_act(dq_act)
        self._set_serial_increment(
            self._rob_def.actuator2serial(dq_act), do_scale=False)
        return dq_act_scaled

    def stop(self):
        self._olimex_in_socket.close()
