# coding=utf-8

"""
Module for base class of all motion controllers. 
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2021"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import inspect
import threading
import logging

import numpy as np


class Controller(object):
    """A controller base class providing some simple common mechanism
    to receive LLC data from the LLC publisher and send joint
    updates to the LLC."""

    class Error(Exception):
        """Error class for thowing exceptions."""

        def __init__(self, message):
            Exception.__init__(self)
            self.message = message
            frame = inspect.stack()[1].frame
            self.caller_class = frame.f_locals['self'].__class__.__name__
            self.caller_method = frame.f_code.co_name

        def __repr__(self):
            return '{}.{} : {}'.format(self.caller_class,
                                       self.caller_method,
                                       self.message)

        def __str__(self):
            return repr(self)

    def __init__(self, **kwargs):
        """Use the given llc publisher for reading input and open a
        socket to write to the LLC on the given 'llchost' address and
        'llcInPort'. Broadcast if 'broadCast' is true.
        """
        self._start_running = kwargs.get('start_running', True)
        self._rob_fac = kwargs.get('robot_facade')
        self._rob_def = self._rob_fac.robot_definition
        self._frame_comp = self._rob_fac.frame_computer
        self._log_level = kwargs.get('log_level', logging.INFO)
        self._log = logging.getLogger(self.__class__.__name__)
        self._log.setLevel(self._log_level)
        self._llc_event = threading.Event()
        self._llc_lock = threading.RLock()
        self._llc_notify_time = -1.0
        self._exit_event = threading.Event()
        self._dq_zero = np.zeros(self._rob_def.dof, dtype=float)
        self._idle_flag = threading.Event()
        self._idle_flag.set()

    def get_is_idle(self):
        """Query the idle-state of the controller. The controller is
        idle when the target twist has been achieved.
        """
        return self._idle_flag.is_set()

    is_idle = property(get_is_idle)

    def wait_for_idle(self, timeout=None):
        """Block until the idle flag is set, signalling that the
        controller has achieved the target twist.
        """
        if timeout is None:
            self._idle_flag.wait()
        else:
            self._idle_flag.wait(timeout)
        return self._idle_flag.is_set()

    def resume(self):
        self._rob_fac.subscribe(self.llc_notify)
        self._log.info('Resumed')

    def suspend(self):
        self._rob_fac.unsubscribe(self.llc_notify)
        self._log.info('Suspended')

    def llc_notify(self, event_time):
        """Standard handler for LLC notification. Status data from the
        LLC are stored in member variable '_llc_notify_data' and the
        '_llc_event' is set.
        """
        with self._llc_lock:
            self._llc_notify_time = event_time
            self._llc_event.set()
