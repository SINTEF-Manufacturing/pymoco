# coding=utf-8
"""
Initialization module for the 'simulator' package.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2012-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"
