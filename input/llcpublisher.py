# coding=utf-8
"""
Module with basic facility for publishing joint configuration packets from an LLC.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2021"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"


import threading
import logging

import numpy as np


class LLCPublisher(threading.Thread):

    class Error(Exception):
        """ Error class for thowing exceptions."""
        
        def __init__(self, message):
            Exception.__init__(self)
            self.message = message

        def __repr__(self):
            return self.__class__.__name__ + ' : ' + self.message

    def __init__(self, **kwargs):
        self._rob_fac = kwargs.get('robot_facade')
        self._rob_def = self._rob_fac.robot_definition
        self._initializing = False
        self._initialized_event = threading.Event()
        self._stop_flag = threading.Event()
        self._p_data = None
        self._p_time = None
        self._p_encoder = np.zeros(self._rob_def.dof, dtype=np.double)
        self._p_actuator = np.zeros(self._rob_def.dof, dtype=np.double)
        self._p_serial = np.zeros(self._rob_def.dof, dtype=np.double)
        self._p_serial_vel = np.zeros(self._rob_def.dof, dtype=np.double)
        self._p_encoder_vel = np.zeros(self._rob_def.dof, dtype=np.double)
        self._p_lock = threading.Lock()
        self._period_est = 0.01
        self._log = logging.getLogger(self.__class__.__name__)
        self._log.setLevel(kwargs.get('log_level', logging.INFO))

    def _notify_subscribers(self, event_time):
        self._rob_fac._event_publisher.publish(event_time)

    def get_period_est(self):
        return self._period_est
    period_est = property(get_period_est)

    def get_serial_joint_velocity(self):
        return self._p_serial_vel
    serial_joint_velocity = property(get_serial_joint_velocity)

    def get_serial_joint_position(self):
        #return self.getStateInfo('serial')
        return self._p_serial
    serial_joint_position = property(get_serial_joint_position)

    def get_actuator_joint_position(self):
        #return self.getStateInfo('actuator')
        return self._p_actuator
    actuator_joint_position = property(get_actuator_joint_position)

    def get_encoder_joint_position(self):
        #return self.getStateInfo('encoder')
        return self._p_encoder
    encoder_joint_position = property(get_encoder_joint_position)

    def get_is_initialized(self):
        return self._initialized_event.isSet()
    is_initialized = property(get_is_initialized)

    def get_is_initializing():
        return self._initializing
    is_initializing = property(get_is_initializing)

    def wait_for_initialized(self):
        self._initialized_event.wait()

    def stop(self):
        """ Stop all activity."""
        self._stop_flag.set()
