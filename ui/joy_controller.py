# coding=utf-8
"""
Module for control class for binding joystick control inputs to
velocity commands of an associated robot.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2012-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import threading
import time

import numpy as np

from .joy_device import JoyDevice

# Left stick axes
lh = 0
lv = 1
# Right stick axes
rh = 2
rv = 3
# Front left buttons
fll = 6
flu = 4
# Front right buttons
frl = 7
fru = 5


class JoyController(threading.Thread):
    def __init__(self, tvc=None, jdev=None,
                 linear_scale=0.005,
                 rotation_scale=0.05):
        threading.Thread.__init__(self)
        self.daemon = True
        self.lin_scl = linear_scale
        self.rot_scl = rotation_scale
        self._control_lock = threading.Lock()
        if tvc is not None:
            self.resume(tvc)
            self._tvc.timeout = 10.0
        else:
            self._tvc = None
            self._suspended = True
        if jdev is None:
            self._jdev = JoyDevice()
            self._jdev.start()
        else:
            self._jdev = jdev
        self.__stop = False

    def run(self):
        while not self.__stop:
            if self._suspended:
                time.sleep(0.1)
                continue
            with self._control_lock:
                self._jdev.wait_for_event()
                axes = self._jdev.axes
                buttons = self._jdev.buttons
                # Prepare scaling factors
                lin_scl = self.lin_scl
                rot_scl = self.rot_scl
                # Check for fine motion
                if buttons[frl]:
                    lin_scl *= 10.0
                    rot_scl *= 10.0
                # Form the twist
                twist = np.zeros(6)
                if buttons[fll]:
                    twist[:3] = lin_scl * axes[[lh, lv, rv]]
                elif buttons[flu]:
                    twist[3:] = rot_scl * axes[[lh, lv, rv]]
                # print(twist)
                expr_frm = 'Base'
                if buttons[fru]:
                    expr_frm = 'Tool'
                self._tvc.set_twist(twist, express_frame=expr_frm)
        if not self._suspended:
            self._tvc.set_twist([0, 0, 0, 0, 0, 0])
            self._tvc.timeout = 0.1

    def suspend(self):
        self._suspended = True
        print('Suspending joystick control. '
              + 'Possibly press a button to wake up the joystick device.')
        with self._control_lock:
            self._tvc.set_twist([0, 0, 0, 0, 0, 0])
            tvc = self._tvc
            self._tvc = None
            tvc.timeout = 0.1
            return tvc

    def resume(self, tvc):
        """Resume operation, using the given 'tvc'."""
        print('Resuming joystick control.')
        with self._control_lock:
            self._suspended = False
            tvc.timeout = 10.0
            self._tvc = tvc

    def get_jdev(self):
        return self._jdev
    jdev = property(get_jdev)

    def stop(self, join=False):
        self.__stop = True
        print('Joystick controller ready for stopping. ' +
              'Press a button to exit.')
        if join:
            self.join()
