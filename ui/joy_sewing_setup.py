#!/usr/bin/env python
"""
Script for starting a simple joystick jogger application for control
of the UR robots of the sewing cell over standard TCP port.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturomg 2012-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import atexit
import code

from pymoco.ui.joy_manager import JoyManager
from pymoco.controller_manager import ControllerManager

cm0 = ControllerManager(
    rob_type='ur',
    rob_host='ur1',
    rob_port=5002,
    log_level=2)

cm1 = ControllerManager(
    rob_type='ur',
    rob_host='ur2',
    rob_port=5002,
    log_level=2)

con_mans = [cm0, cm1]

for cm in con_mans:
    atexit.register(cm.stop)

jm = JoyManager(con_mans)

code.interact(None, None, globals())
