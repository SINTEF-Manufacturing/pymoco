# coding=utf-8
"""
Backward compatibility module. Use 'frame_computer' instead.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"



print('!!! Deprecation Warning !!!: Module name change "framecomputer" -> "frame_computer"')
from .frame_computer import *
